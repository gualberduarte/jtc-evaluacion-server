package py.com.jtc.rest.repository;

import java.util.List;
import py.com.jtc.rest.bean.Cuenta;

public interface CuentaRepository {

	List<Cuenta> obtenerCuentas();
	
	Cuenta obtenerCuenta(Integer id);
	
	Cuenta agregarCuenta(Cuenta cuenta);
	
	boolean eliminarCuenta(Integer id);
	
	boolean actualizarCuenta(Cuenta cuenta, Integer id);
	
}