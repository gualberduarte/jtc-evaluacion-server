package py.com.jtc.rest.repository;

import java.util.List;
import py.com.jtc.rest.bean.Usuario;

public interface UsuarioRepository {
	
	List<Usuario> obtenerUsuarios();
	
	Usuario obtenerUsuario(Integer id);
	
	Usuario agregarUsuario(Usuario usuario);
	
	boolean eliminarUsuario(Integer id);
	
	boolean actualizarUsuario(Usuario usuario, Integer id);
	
}