package py.com.jtc.rest.repository;

import java.util.List;
import py.com.jtc.rest.bean.UsuarioCuenta;

public interface UsuarioCuentaRepository {

	List<UsuarioCuenta> obtenerUsuariosCuentas();
	
	UsuarioCuenta obtenerUsuarioCuenta(Integer id);
	
	UsuarioCuenta agregarUsuarioCuenta(UsuarioCuenta usuarioCuenta);
	
	boolean eliminarUsuarioCuenta(Integer id);
	
	boolean actualizarUsuarioCuenta(UsuarioCuenta usuarioCuenta, Integer id);
	
}