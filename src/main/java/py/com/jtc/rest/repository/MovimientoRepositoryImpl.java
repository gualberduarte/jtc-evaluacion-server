package py.com.jtc.rest.repository;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import py.com.jtc.rest.bean.Movimiento;

@Repository
public class MovimientoRepositoryImpl implements MovimientoRepository{

	@Autowired
	private JdbcTemplate jdbcTemplate;
		
	@Override
	public List<Movimiento> obtenerMovimientos() {
		List<Movimiento> movimientos = jdbcTemplate.query("select * from movimientos", 
				(rs) -> {
					List<Movimiento> list = new ArrayList<>();
					while(rs.next()){
						Movimiento m = new Movimiento();						
						m.setIdMovimiento(rs.getInt("idMovimiento"));
						m.setFechaHora(rs.getTimestamp("fechaHora"));
						m.setIdCuenta(rs.getInt("idCuenta"));
						m.setTipoMovimiento(rs.getString("tipoMovimiento"));
						m.setMonto(rs.getInt("monto"));
						list.add(m);
					}
					return list;
				});
		return movimientos;
	}
	
	@Override
	public Movimiento obtenerMovimiento(Integer id) {
		Movimiento movimiento = jdbcTemplate.queryForObject("select * from movimientos where idmovimiento = ?", 
				new Object[] {id}, 
				(rs, rowNum) -> {
					Movimiento m = new Movimiento();
					m.setIdMovimiento(rs.getInt("idMovimiento"));
					m.setFechaHora(rs.getTimestamp("fechaHora"));
					m.setIdCuenta(rs.getInt("idCuenta"));
					m.setTipoMovimiento(rs.getString("tipoMovimiento"));
					m.setMonto(rs.getInt("monto"));
					return m;
		        });
		
		return movimiento;
	}

	@Override
	public Movimiento agregarMovimiento(Movimiento movimiento) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		PreparedStatementCreator psc = (conn) -> {
			PreparedStatement ps = conn.prepareStatement("insert into movimientos values (?,?,?,?,?)", 
					Statement.RETURN_GENERATED_KEYS);
			
			ps.setNull(1, Types.INTEGER);
			ps.setDate(2, (Date) movimiento.getFechaHora());
			ps.setInt(3,  movimiento.getIdCuenta());
			ps.setString(4, movimiento.getTipoMovimiento());
			ps.setInt(5,  movimiento.getMonto());
	        return ps;
		}; 
		
		int result = jdbcTemplate.update(psc, keyHolder);
		
		if (result > 0)
			movimiento.setIdMovimiento(keyHolder.getKey().intValue());
		
		return movimiento;
		
	}

	@Override
	public boolean eliminarMovimiento(Integer id) {
		int result = jdbcTemplate.update("delete from movimientos where idmovimiento = ?", id);
		
		return (result > 0) ? true : false;
	}

	@Override
	public boolean actualizarMovimiento(Movimiento movimiento, Integer id) {
		int result = jdbcTemplate.update("update movimientos set fechahora = ?, idcuenta = ?, tipomovimiento = ?, monto = ? where idmovimiento = ?", 
				movimiento.getFechaHora(), movimiento.getIdCuenta(), movimiento.getTipoMovimiento(), movimiento.getMonto(), id);
		return (result > 0) ? true : false;
	}

}