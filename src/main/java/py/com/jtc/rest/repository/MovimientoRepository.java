package py.com.jtc.rest.repository;

import java.util.List;
import py.com.jtc.rest.bean.Movimiento;

public interface MovimientoRepository {

	List<Movimiento> obtenerMovimientos();
	
	Movimiento obtenerMovimiento(Integer id);
	
	Movimiento agregarMovimiento(Movimiento movimiento);
	
	boolean eliminarMovimiento(Integer id);
	
	boolean actualizarMovimiento(Movimiento movimiento, Integer id);
	
}