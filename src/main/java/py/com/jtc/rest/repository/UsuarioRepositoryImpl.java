package py.com.jtc.rest.repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import py.com.jtc.rest.bean.Usuario;

@Repository
public class UsuarioRepositoryImpl implements UsuarioRepository{

	@Autowired
	private JdbcTemplate jdbcTemplate;
		
	@Override
	public List<Usuario> obtenerUsuarios() {
		List<Usuario> usuarios = jdbcTemplate.query("select * from usuarios", 
				(rs) -> {
					List<Usuario> list = new ArrayList<>();
					while(rs.next()){
						Usuario u = new Usuario();
						u.setIdUsuario(rs.getInt("idUsuario"));
						u.setNroDocumento(rs.getString("nroDocumento"));
						u.setNombre(rs.getString("nombre"));
						u.setPin(rs.getString("pin"));
						list.add(u);
					}
					return list;
				});
		return usuarios;
	}
	
	@Override
	public Usuario obtenerUsuario(Integer id) {
		Usuario usuario = jdbcTemplate.queryForObject("select * from usuarios where idusuario = ?", 
				new Object[] {id}, 
				(rs, rowNum) -> {
					Usuario u = new Usuario();
					u.setIdUsuario(rs.getInt("idUsuario"));
					u.setNroDocumento(rs.getString("nroDocumento"));
					u.setNombre(rs.getString("nombre"));
					u.setPin(rs.getString("pin"));
					return u;
		        });
		
		return usuario;
	}

	@Override
	public Usuario agregarUsuario(Usuario usuario) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		PreparedStatementCreator psc = (conn) -> {
			PreparedStatement ps = conn.prepareStatement("insert into usuarios values (?,?,?,?)", 
					Statement.RETURN_GENERATED_KEYS);
			
			ps.setNull(1, Types.INTEGER);
			ps.setString(2, usuario.getNroDocumento());
			ps.setString(3, usuario.getNombre());
			ps.setString(4, usuario.getPin());
	        return ps;
		}; 
		
		int result = jdbcTemplate.update(psc, keyHolder);
		
		if (result > 0)
			usuario.setIdUsuario(keyHolder.getKey().intValue());
		
		return usuario;
		
	}

	@Override
	public boolean eliminarUsuario(Integer id) {
		int result = jdbcTemplate.update("delete from usuarios where idusuario = ?", id);
		
		return (result > 0) ? true : false;
	}

	@Override
	public boolean actualizarUsuario(Usuario usuario, Integer id) {
		int result = jdbcTemplate.update("update usuarios set nrodocumento=? nombre = ?, pin = ? where idusuario = ?", 
				usuario.getNroDocumento(), usuario.getNombre(), usuario.getPin(), id);
		return (result > 0) ? true : false;
	}

}