package py.com.jtc.rest.repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import py.com.jtc.rest.bean.UsuarioCuenta;

@Repository
public class UsuarioCuentaRepositoryImpl implements UsuarioCuentaRepository{

	@Autowired
	private JdbcTemplate jdbcTemplate;
		
	@Override
	public List<UsuarioCuenta> obtenerUsuariosCuentas() {
		List<UsuarioCuenta> usuariosCuentas = jdbcTemplate.query("select * from usuarios_cuentas", 
				(rs) -> {
					List<UsuarioCuenta> list = new ArrayList<>();
					while(rs.next()){
						UsuarioCuenta uc = new UsuarioCuenta();
						uc.setIdFirma(rs.getInt("idFirma"));
						uc.setIdUsuario(rs.getInt("idUsuario"));
						uc.setIdCuenta(rs.getInt("idCuenta"));
						list.add(uc);
					}
					return list;
				});
		return usuariosCuentas;
	}
	
	@Override
	public UsuarioCuenta obtenerUsuarioCuenta(Integer id) {
		UsuarioCuenta usuarioCuenta = jdbcTemplate.queryForObject("select * from usuarios_ccuentas where idfirma = ?", 
				new Object[] {id}, 
				(rs, rowNum) -> {
					UsuarioCuenta uc = new UsuarioCuenta();
					uc.setIdFirma(rs.getInt("idFirma"));
					uc.setIdUsuario(rs.getInt("idUsuario"));
					uc.setIdCuenta(rs.getInt("idCuenta"));
					return uc;
		        });
		
		return usuarioCuenta;
	}

	@Override
	public UsuarioCuenta agregarUsuarioCuenta(UsuarioCuenta usuarioCuenta) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		PreparedStatementCreator psc = (conn) -> {
			PreparedStatement ps = conn.prepareStatement("insert into usuarios_cuentas values (?,?,?)", 
					Statement.RETURN_GENERATED_KEYS);
			
			ps.setNull(1, Types.INTEGER);
			ps.setInt(2, usuarioCuenta.getIdUsuario());
			ps.setInt(3, usuarioCuenta.getIdCuenta());
	        return ps;
		}; 
		
		int result = jdbcTemplate.update(psc, keyHolder);
		
		if (result > 0)
			usuarioCuenta.setIdFirma(keyHolder.getKey().intValue());
		
		return usuarioCuenta;
		
	}

	@Override
	public boolean eliminarUsuarioCuenta(Integer id) {
		int result = jdbcTemplate.update("delete from usuarios_cuentas where idfirma = ?", id);
		
		return (result > 0) ? true : false;
	}

	@Override
	public boolean actualizarUsuarioCuenta(UsuarioCuenta usuarioCuenta, Integer id) {
		int result = jdbcTemplate.update("update usuarios_cuentas set idusuario=? idcuenta = ?, pin = ? where idfirma = ?", 
				usuarioCuenta.getIdUsuario(), usuarioCuenta.getIdCuenta(), id);
		return (result > 0) ? true : false;
	}

}