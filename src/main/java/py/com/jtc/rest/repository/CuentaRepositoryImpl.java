package py.com.jtc.rest.repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import py.com.jtc.rest.bean.Cuenta;


@Repository
public class CuentaRepositoryImpl implements CuentaRepository{

	@Autowired
	private JdbcTemplate jdbcTemplate;
		
	@Override
	public List<Cuenta> obtenerCuentas() {
		
		List<Cuenta> cuentas = jdbcTemplate.query("select * from cuentas", 
				(rs) -> {
					List<Cuenta> list = new ArrayList<>();
					while(rs.next()){
						Cuenta c = new Cuenta();
						c.setIdCuenta(rs.getInt("idCuenta"));
						c.setNroCuenta(rs.getString("nroCuenta"));
						c.setTipo(rs.getString("tipo"));
						c.setMoneda(rs.getString("moneda"));
						c.setSaldo(rs.getInt("saldo"));
						list.add(c);
					}
					return list;
				});
		return cuentas;
	}
	
	@Override
	public Cuenta obtenerCuenta(Integer id) {
		Cuenta cuenta = jdbcTemplate.queryForObject("select * from cuentas where idcuenta = ?", 
				new Object[] {id}, 
				(rs, rowNum) -> {
					Cuenta c = new Cuenta();
					c.setIdCuenta(rs.getInt("idCuenta"));
					c.setNroCuenta(rs.getString("nroCuenta"));
					c.setTipo(rs.getString("tipo"));
					c.setMoneda(rs.getString("moneda"));
					c.setSaldo(rs.getInt("saldo"));
					return c;
		        });
		
		return cuenta;
	}

	@Override
	public Cuenta agregarCuenta(Cuenta cuenta) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		
		PreparedStatementCreator psc = (conn) -> {
			PreparedStatement ps = conn.prepareStatement("insert into cuentas values (?,?,?,?,?)", 
					Statement.RETURN_GENERATED_KEYS);
			
			ps.setNull(1, Types.INTEGER);
			ps.setString(2, cuenta.getNroCuenta());
			ps.setString(3, cuenta.getTipo());
			ps.setString(4, cuenta.getMoneda());
			ps.setInt(5, cuenta.getSaldo());
			
	        return ps;
		}; 
		
		int result = jdbcTemplate.update(psc, keyHolder);
		
		if (result > 0)
			cuenta.setIdCuenta(keyHolder.getKey().intValue());
		
		return cuenta;
		
	}

	@Override
	public boolean eliminarCuenta(Integer id) {
		int result = jdbcTemplate.update("delete from cuentas where idcuenta = ?", id);
		
		return (result > 0) ? true : false;
	}

	@Override
	public boolean actualizarCuenta(Cuenta cuenta, Integer id) {
		int result = jdbcTemplate.update("update cuentas set nrocuenta=? tipo = ?, moneda = ?, saldo = ? where idcuenta = ?", 
				cuenta.getNroCuenta(), cuenta.getTipo(), cuenta.getMoneda(), cuenta.getSaldo(), id);
		return (result > 0) ? true : false;
	}

}