package py.com.jtc.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import py.com.jtc.rest.bean.Login;
import py.com.jtc.rest.bean.Usuario;

@RestController
@RequestMapping("/login")
public class LoginController {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@PostMapping
	public boolean login(@RequestBody Login login) {
		System.out.println("LoginController server");
		System.out.println("username " + login.getUsername());
		System.out.println("password " + login.getPassword());
		
		Usuario usuario = jdbcTemplate.queryForObject("select * from usuarios where nrodocumento = ?", 
				new Object[] {login.getUsername()}, 
				(rs, rowNum) -> {
					Usuario u = new Usuario();
					u.setIdUsuario(rs.getInt("idusuario"));
					u.setNroDocumento(rs.getString("nrodocumento"));
					u.setNombre(rs.getString("nombre"));
					u.setPin(rs.getString("pin"));
					return u;
		        });
		
		System.out.println("usuario " + usuario.toString());
		
		if (usuario != null) {
			if (usuario.getPin().equals(login.getPassword())){
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
		
	}

}