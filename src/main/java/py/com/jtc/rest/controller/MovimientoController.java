package py.com.jtc.rest.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import py.com.jtc.rest.bean.Movimiento;
import py.com.jtc.rest.repository.MovimientoRepository;


@RestController
@RequestMapping("/movimientos")
public class MovimientoController {
	
	@Autowired
	private MovimientoRepository movimientoRepository;
	
	@GetMapping
	public List<Movimiento> obtenerMovimientos() {
		List<Movimiento> movimientos = movimientoRepository.obtenerMovimientos();
		return movimientos;
	}
	
	@GetMapping("/{id}")
	public Movimiento obtenerMovimiento(@PathVariable("id") String id) {
		Movimiento movimiento = movimientoRepository.obtenerMovimiento(Integer.valueOf(id));
		return movimiento; 
	}
	
	@PostMapping
	public ResponseEntity<Movimiento> guardarMovimiento(@RequestBody Movimiento movimiento, UriComponentsBuilder uBuilder) {
		Movimiento m = movimientoRepository.agregarMovimiento(movimiento);
		HttpHeaders header = new HttpHeaders();
		
		URI uri = uBuilder.path("/movimientos/" )
				.path(String.valueOf(movimiento.getIdMovimiento()))
				.build()
				.toUri();
		
		header.setLocation(uri);
		
		return new ResponseEntity<>(m, header, HttpStatus.CREATED);
	}
	
	@PutMapping("/{id}")
	public Movimiento editarMovimiento(@PathVariable("id") String id,
			@RequestBody Movimiento movimiento, 
			UriComponentsBuilder uBuilder) 
	{
		Movimiento m = null;
		boolean result = movimientoRepository.actualizarMovimiento(movimiento, Integer.valueOf(id));
		if (result) {
			 m = obtenerMovimiento(id);
		}
		return m;
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> eliminarMovimiento(@PathVariable("id") String id) { 
		boolean delete = movimientoRepository.eliminarMovimiento(Integer.valueOf(id));
		return new ResponseEntity<Movimiento> (HttpStatus.NO_CONTENT); //ResponseEntity
	}


}