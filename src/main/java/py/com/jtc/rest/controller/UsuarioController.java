package py.com.jtc.rest.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import py.com.jtc.rest.bean.Usuario;
import py.com.jtc.rest.repository.UsuarioRepository;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@GetMapping
	public List<Usuario> obtenerUsuarios() {
		List<Usuario> usuarios = usuarioRepository.obtenerUsuarios();
		return usuarios;
	}
	
	@GetMapping("/{id}")
	public Usuario obtenerUsuario(@PathVariable("id") String id) {
		Usuario usuario = usuarioRepository.obtenerUsuario(Integer.valueOf(id));
		return usuario; 
	}
	
	@PostMapping
	public ResponseEntity<Usuario> guardarUsuario(@RequestBody Usuario usuario, UriComponentsBuilder uBuilder) {
		Usuario u = usuarioRepository.agregarUsuario(usuario);
		HttpHeaders header = new HttpHeaders();
		
		URI uri = uBuilder.path("/usuarios/" )
				.path(String.valueOf(usuario.getIdUsuario()))
				.build()
				.toUri();
		
		header.setLocation(uri);
		
		return new ResponseEntity<>(u, header, HttpStatus.CREATED);
	}
	
	@PutMapping("/{id}")
	public Usuario editarUsuario(@PathVariable("id") String id,
			@RequestBody Usuario usuario, 
			UriComponentsBuilder uBuilder) 
	{
		Usuario u = null;
		boolean result = usuarioRepository.actualizarUsuario(usuario, Integer.valueOf(id));
		if (result) {
			 u = obtenerUsuario(id);
		}
		return u;
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> eliminarUsuario(@PathVariable("id") String id) { 
		boolean delete = usuarioRepository.eliminarUsuario(Integer.valueOf(id));
		return new ResponseEntity<Usuario> (HttpStatus.NO_CONTENT); //ResponseEntity
	}


}