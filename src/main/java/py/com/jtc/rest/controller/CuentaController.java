package py.com.jtc.rest.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import py.com.jtc.rest.bean.Cuenta;
import py.com.jtc.rest.repository.CuentaRepository;
import py.com.jtc.rest.utils.LogUtil;

@RestController
@RequestMapping("/cuentas")
public class CuentaController {

	@Autowired
	private CuentaRepository cuentaRepository;
	
	@GetMapping
	public List<Cuenta> obtenerCuentas() {
		System.out.println("obtenerCuentas");
		LogUtil.INFO("Iniciando obtenerCuentas");
		List<Cuenta> cuentas = cuentaRepository.obtenerCuentas();
		LogUtil.INFO("Response: " + cuentas.toString());
		return cuentas; 
	}
	
	@GetMapping("/{id}")
	public Cuenta obtenerCuenta(@PathVariable("id") String id) {
		LogUtil.INFO("Iniciando obtenerCuenta " + id);
		Cuenta cuenta = cuentaRepository.obtenerCuenta(Integer.valueOf(id));
		LogUtil.INFO("Response: " + cuenta.toString());
		return cuenta;
	}
	
	@PostMapping
	public ResponseEntity<Cuenta> guardarCuenta(@RequestBody Cuenta cuenta, UriComponentsBuilder uBuilder) {
		LogUtil.INFO("Iniciando guardarCuenta " + cuenta);
		Cuenta c = cuentaRepository.agregarCuenta(cuenta);
		HttpHeaders header = new HttpHeaders();
		
		URI uri = uBuilder.path("/cuentas/" )
				.path(String.valueOf(cuenta.getIdCuenta()))
				.build()
				.toUri();
		
		header.setLocation(uri);
		LogUtil.INFO("Response: " + c.toString());
		return new ResponseEntity<>(c, header, HttpStatus.CREATED);
	}
	
	@PutMapping("/{id}")
	public Cuenta editarCuenta(@PathVariable("id") String id,
			@RequestBody Cuenta cuenta, 
			UriComponentsBuilder uBuilder) 
	{
		LogUtil.INFO("Iniciando editarCuenta " + cuenta.toString());
		Cuenta c = null;
		boolean result = cuentaRepository.actualizarCuenta(cuenta, Integer.valueOf(id));
		if (result) {
			 c = obtenerCuenta(id);
		}
		LogUtil.INFO("Response: " + c.toString());
		return c;
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> eliminarCuenta(@PathVariable("id") String id) { 
		LogUtil.INFO("Iniciando eliminarCuenta " + id);
		boolean delete = cuentaRepository.eliminarCuenta(Integer.valueOf(id));
		LogUtil.INFO("Response: " + delete);
		return new ResponseEntity<Cuenta> (HttpStatus.NO_CONTENT); //ResponseEntity
	}
}