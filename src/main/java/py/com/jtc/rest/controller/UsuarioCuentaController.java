package py.com.jtc.rest.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import py.com.jtc.rest.bean.UsuarioCuenta;
import py.com.jtc.rest.repository.UsuarioCuentaRepository;

@RestController
@RequestMapping("/usuarios_cuentas")
public class UsuarioCuentaController {
	
	@Autowired
	private UsuarioCuentaRepository usuarioCuentaRepository;
	
	@GetMapping
	public List<UsuarioCuenta> obtenerUsuariosCuentas() {
		List<UsuarioCuenta> usuariosCuentas = usuarioCuentaRepository.obtenerUsuariosCuentas();
		return usuariosCuentas;
	}
	
	@GetMapping("/{id}")
	public UsuarioCuenta obtenerUsuarioCuenta(@PathVariable("id") String id) {
		UsuarioCuenta usuarioCuenta = usuarioCuentaRepository.obtenerUsuarioCuenta(Integer.valueOf(id));
		return usuarioCuenta; 
	}
	
	@PostMapping
	public ResponseEntity<UsuarioCuenta> guardarUsuarioCuenta(@RequestBody UsuarioCuenta usuarioCuenta, UriComponentsBuilder uBuilder) {
		UsuarioCuenta uc = usuarioCuentaRepository.agregarUsuarioCuenta(usuarioCuenta);
		HttpHeaders header = new HttpHeaders();
		
		URI uri = uBuilder.path("/usuarios_cuentas/" )
				.path(String.valueOf(usuarioCuenta.getIdFirma()))
				.build()
				.toUri();
		
		header.setLocation(uri);
		
		return new ResponseEntity<>(uc, header, HttpStatus.CREATED);
	}
	
	@PutMapping("/{id}")
	public UsuarioCuenta editarUsuarioCuenta(@PathVariable("id") String id,
			@RequestBody UsuarioCuenta usuario, 
			UriComponentsBuilder uBuilder) 
	{
		UsuarioCuenta uc = null;
		boolean result = usuarioCuentaRepository.actualizarUsuarioCuenta(usuario, Integer.valueOf(id));
		if (result) {
			 uc = obtenerUsuarioCuenta(id);
		}
		return uc;
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> eliminarUsuarioCuenta(@PathVariable("id") String id) { 
		boolean delete = usuarioCuentaRepository.eliminarUsuarioCuenta(Integer.valueOf(id));
		return new ResponseEntity<UsuarioCuenta> (HttpStatus.NO_CONTENT); //ResponseEntity
		
	}


}